package homework4;

public class HumanBeforeRefactoring {

    private    String name;
    private    String surname;
    private    int year;
    private    byte iq;
    private    Pet pet;
    private    HumanBeforeRefactoring mother;
     private   HumanBeforeRefactoring father;
     private   Family family;
    private    String [][] schedule;

    public   String  getName(){
        return  name;

    }
    public void setName(String humanName){
        name = humanName;
    }
    public  String  getSurname(){
        return  surname;

    }
    public void setSurname(String humanSurname){
        surname = humanSurname;
    }
    public  int  getYear(){
        return  year;
    }
    public void setYear(int humanYear){
        year = humanYear;
    }
    public  Pet  getPet(){
        return  pet;
    }
    public void setPet(Pet humanPet){
        pet = humanPet;
    }
    public  HumanBeforeRefactoring  getMother(){
        return  mother;
    }
    public void setMother(HumanBeforeRefactoring humanMother){
        mother = humanMother;
    }
    public  HumanBeforeRefactoring  getFather(){
        return  father;
    }
    public void setFather(HumanBeforeRefactoring humanFather){
        father= humanFather;
    }
    public  byte  getIq(){
        return  iq;
    }
    public void setIq(byte humanIq){
        iq = humanIq;
    }
    public  HumanBeforeRefactoring(){}

       public  HumanBeforeRefactoring( String name, String surname, int year){
         this.name =name;
        this.surname = surname;
        this.year = year;
      }

      public HumanBeforeRefactoring(String name, String surname, int age, HumanBeforeRefactoring mother, HumanBeforeRefactoring father){
         this.name = name;
     this.surname = surname;
         this.year = year;
      this.mother = mother;
     this.father = father;
     }

      public HumanBeforeRefactoring(String name, String surname, int age, Pet pet, HumanBeforeRefactoring mother, HumanBeforeRefactoring father, String [][] schedule){
        this.name = name;
         this.surname = surname;
         this.year = year;
      this.pet = pet;
       this.mother = mother;
        this.father = father;
       this.schedule = schedule;
     }
    public  void greetPet ( ){
        System.out.println("Привет " + this.pet.getNickName() );
    }
    public  static void  describePet(Pet pet){
        System.out.println( "У меня есть " + pet.getSpecies() +", ему "+ pet.getAge() + " лет, он "+ (pet.getTrickLevel() > 50? "очень хитрый":"почти не хитрый"));
    }
      public  String toString (){
         String message = "Human{name = " + name + " surname = " + surname +" year = "+ year + " iq = " + iq + " mother =" + mother.getName() + mother.getSurname()
               + " father =" +  father.getName() + father.getSurname() + " pet= " + pet.toString();
        System.out.println(message);
        return message;
     }
    public static void main(String[] args) {
            Pet Mura = new Pet("cat","Mura");
            Mura.setAge(10);
            Mura.setTrickLevel((byte) 40);
            HumanBeforeRefactoring John = new HumanBeforeRefactoring();
            John.setPet(Mura);
            John.greetPet();
            John.describePet(Mura);
            HumanBeforeRefactoring Sam = new HumanBeforeRefactoring("Sam","Smith",32);
            Sam.setPet(Mura);
            Sam.setSurname("Smith");
            John.setName( "John");
            John.setSurname("Smith");
            John.setYear(1959);
            HumanBeforeRefactoring Jessica = new HumanBeforeRefactoring("Jessica","Smith",50);
            Sam.setMother(Jessica);
            Sam.setFather(John);
            String [] habits = new String[]{"eat","drink","sleep"};
            Mura.setHabits(habits);
            John.setIq((byte)110);
            Sam.setIq((byte) 120);
            Sam.setYear( 1989);


            Sam.greetPet();
            Sam.toString();
    }

}
