package homework4;

import java.util.Arrays;

class Human {

    private    String name;
    private    String surname;
    private    int year;
    private    byte iq;
    private    Family family;
    private    String [][] schedule;

    public  Human(){

    }

    public  Human( String name, String surname, int year){
        this.name =name;
        this.surname = surname;
        this.year = year;
    }

    public  Human(String name,String surname,int year,Family family){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }

    public  Human(String name,String surname,int year,Family family,String [][] shedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
        this.schedule = shedule;
    }
    public   String  getName(){
        return  name;

    }

    public void setName(String humanName){

        name = humanName;
    }

    public  String  getSurname(){
        return  surname;

    }
    public void setSurname(String humanSurname){

        surname = humanSurname;
    }

    public  int  getYear(){

        return  year;
    }

    public void setYear(int humanYear){

        year = humanYear;
    }


    public Family getFamily(){
        return family;
    }

    public  void setFamily(Family humanFamily){
        family = humanFamily;

    }
    public String[][] getSchedule(){
        return schedule;
    }
    public  void setSchedule (String[][] humanSchedule){
        schedule = humanSchedule;
    }

    public  void greetPet ( Family family){

        System.out.println("Привет " + family.getPet().getNickName() );
    }
    public   void  describePet(Family family){

        System.out.println( "У меня есть " + family.getPet().getSpecies() +", ему "+ family.getPet().getAge() + " лет, он "+ (family.getPet().getTrickLevel() > 50? "очень хитрый":"почти не хитрый"));
    }

    @Override
    public  String toString (){
        String message = "Human{name = " + name + " surname = " + surname +" year = "+ year + " iq = " + iq + " schedule = " + Arrays.deepToString(schedule) +
                "}" ;
        System.out.println(message);
        return message;
    }
    @Override
    public int hashCode(){
        int result = this.getSurname() == null?0:this.getSurname().hashCode();
        result = result + year;
        result = result + iq;
        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Human.class)){
            return false;
        }
        Human human = (Human) obj;
       String humanName = human.getName();
       String humanSurname = human.getSurname();
       Family humanFamily = human.getFamily();
        if(humanName == this.name  &&
                ( humanSurname == this.surname ||  humanSurname.equals(this.surname))  &&
                (humanFamily == this.family || humanFamily.equals(this.family))) {
            return true;
        }else  return false;

    }

    public static void main(String[] args) {
        Pet Mura = new Pet("Mura","Mura");
        Mura.setAge(10);
        Mura.setTrickLevel((byte) 40);
        Human John = new Human();

        Human Sam = new Human("Sam","Smith",32);

        Sam.surname ="Smith";
        John.name ="John";
        John.surname = "Smith";
        Human Jessica = new Human("Jessica","Smith",50);
        Family Johnson = new Family(Jessica,John);
        String [] habits = new String[]{"eat","drink","sleep"};
        String [][] schedule =new String[][]{{"Monday","ffkot"},{"Tuesday","yggres"}};
        Sam.schedule = schedule;
        Sam.family = Johnson;

        Mura.setHabits(habits);
        John.iq = 110;
        Sam.iq =120;
        Sam.year = 1989;
        John.year = 1959;
        Johnson.setPet(Mura);
        John.greetPet(John.family);
        John.describePet(John.family);
        Sam.greetPet(Sam.family);
        Sam.toString();
    }

}