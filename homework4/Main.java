package homework4;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Human Betsy =new Human();
        Betsy.toString();
        Human Mary = new Human("Mary","Johnson",1978);
        Human William = new Human("william","Johnson",1976);
        Human Sam =new Human("Sam","Smith",1960);
        Human Jane = new Human("Jane","Smith",1965);
        Family Smith = new Family(Jane,Sam);
        Family Johnson = new Family(Mary,William);
        Pet Mura = new Pet("cat","Mura",5,(byte) 50);
        Pet Linda = new Pet("dog","Linda");
        Linda.toString();
        String [] habits =  new String [] {"eat","drink","sleep"};
        Mura.setHabits(habits);
        Human Dorothy = new Human("Dorothy","Johnson",1998,Johnson);
        Human Michael = new Human("Michael","Johnson",2001);

        Johnson.setPet(Mura);
        String [][] schedule1 =new String[][]{{"Monday","cook dinner"},{"Tuesday","help to do home tasks"}};
        String [][] schedule2 =new String[][]{{"Monday","fix car"},{"Tuesday","go to the gym"}};
        String [][] schedule3 =new String[][]{{"Monday","do homework"},{"Tuesday","pass exams"}};

        Mary.setSchedule(schedule1);
        William.setSchedule(schedule2);
        Dorothy.setSchedule(schedule3);
        Michael.setSchedule(schedule3);
        Human Mike =new Human("Michael","Johnson",2001, Johnson,schedule2);
        Human Julia =new Human("Julia","Johnson",2002, Johnson,schedule1);
        Sam.setSchedule(schedule2);
        Jane.setSchedule(schedule1);
        Smith.setPet(Mura);
        System.out.println("------------------------------------------");
        Smith.toString();
        Johnson.addChild(Dorothy);
        Johnson.addChild(Michael);
        Johnson.addChild(Julia);

        System.out.println(Johnson.getChildren().length);
        System.out.println(Michael.equals(Mike));

        System.out.println("------------------------------------------");
        Johnson.toString();
        System.out.println("------------------------------------------");

        Johnson.deleteChild(1);
        System.out.println(Johnson.getChildren().length);

        Johnson.toString();

        System.out.println(Johnson.hashCode());


    }
}
